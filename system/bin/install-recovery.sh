#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:67108864:e589d75e8ebf124a62b328ce4a0538c3ab57e95e; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:67108864:101f97abcba36f046fa62758424a4070a3bb19c5 EMMC:/dev/block/bootdevice/by-name/recovery e589d75e8ebf124a62b328ce4a0538c3ab57e95e 67108864 101f97abcba36f046fa62758424a4070a3bb19c5:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
